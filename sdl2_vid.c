#include "doomfire.h"

#include <SDL2/SDL.h>



SDL_Window *Window;
SDL_Renderer *Renderer;

int vid_software_vsync = 0;

typedef struct
{
	SDL_Texture *Texture;
	int Width;
	int Height;
} r_surface;

void VID_Init(const char *Title, int x, int y, int Width, int Height, int Resizable, int Centered)
{
	SDL_Init(SDL_INIT_VIDEO);
	int WindowFlags = 0;

	if(Centered)
	{
		x = SDL_WINDOWPOS_CENTERED;
		y = SDL_WINDOWPOS_CENTERED;
	}

	if(Resizable)
	{
		WindowFlags |= SDL_WINDOW_RESIZABLE;
	}

	Window = SDL_CreateWindow(Title, x, y, Width, Height, WindowFlags);
	Renderer = SDL_CreateRenderer(Window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
}

void VID_Shutdown()
{
	SDL_DestroyRenderer(Renderer);
	SDL_DestroyWindow(Window);
}

r_surface *R_CreateSurface(int Width, int Height)
{
	r_surface *Surface = malloc(sizeof *Surface);
	Surface->Width = Width;
	Surface->Height = Height;
	Surface->Texture = SDL_CreateTexture(Renderer, SDL_PIXELFORMAT_BGRA32, SDL_TEXTUREACCESS_STREAMING, Width, Height);

	return Surface;
}

// Note: Do not free the memory returned by this function
void R_GetSurfacePixels(r_surface *Surface, void **Destination)
{
	int Pitch = 0;
	SDL_LockTexture(Surface->Texture, 0, Destination, &Pitch);
	assert(Pitch == Surface->Width * 4);
}

void R_ReleaseSurfacePixels(r_surface *Surface)
{
	SDL_UnlockTexture(Surface->Texture);
}

void R_BlitSurface(r_surface *Src, r_surface *Dst)
{
	// Dst is not used here
	SDL_RenderCopyEx(Renderer, Src->Texture, 0, 0, 0, 0, SDL_FLIP_NONE);
}

void VID_SwapBuffers()
{
	SDL_RenderPresent(Renderer);
}


void VID_PollEvents()
{
	SDL_Event Event;
	while(SDL_PollEvent(&Event))
	{
		switch(Event.type)
		{
			case SDL_QUIT:
				Running = 0;
			break;

			case SDL_KEYDOWN:
			{
				int key = Event.key.keysym.sym;
				ks_event Event;
				Event.key = DFK_NONE;
				Event.pressed = 1;
				switch(key)
				{
					case SDLK_q:
					case SDLK_ESCAPE:
						Event.key = DFK_QUIT;
						break;
					case SDLK_c:
						Event.key = DFK_DOUSE;
						break;
					case SDLK_v:
						Event.key = DFK_IGNITE;
						break;
				}
				KS_SendKey(&Event);
			}
			break;
		}
	}
}
