#include "doomfire.h"

#include <X11/Xlib.h>
#include <X11/Xutil.h>

#include <X11/XKBlib.h>
#include <X11/keysym.h>

int vid_software_vsync = 1;

typedef struct
{
	XImage *Texture;
	unsigned char *Pixels;
	int Width;
	int Height;
} r_surface;

typedef struct
{
	Display *Dpy;
	Window Window;
	Window Root;
	XWindowAttributes Attributes;
	GC Gc;

	// For software scaling, if needed
	r_surface *Screen;

} x11_window;

x11_window Win;


r_surface *R_CreateSurface(int Width, int Height);

void VID_Init(const char *Title, int x, int y, int Width, int Height, int Resizable, int Centered)
{
	Win.Dpy = XOpenDisplay(0);
	Win.Root = XDefaultRootWindow(Win.Dpy);

	if(Centered)
	{
		Screen *Screen = ScreenOfDisplay(Win.Dpy, DefaultScreen(Win.Dpy));
		x = (Screen->width / 2) - (Width / 2); 
		y = (Screen->height / 2) - (Height / 2); 
	}

	Win.Window = XCreateSimpleWindow(Win.Dpy, Win.Root, x, y, Width, Height, 0, 0, 0);

	// Window title setup
	XTextProperty WindowText;
	XStringListToTextProperty((char **)&Title, 1, &WindowText);
	XSetWMName(Win.Dpy, Win.Window, &WindowText);

	// Restrict window dimensions
	if(!Resizable)
	{
		XSizeHints SizeHints = {0};
		SizeHints.flags = PSize | PMinSize | PMaxSize;
		SizeHints.width = Width;
		SizeHints.height = Height;
		SizeHints.min_width = Width;
		SizeHints.min_height = Height;
		SizeHints.max_width = Width;
		SizeHints.max_height = Height;
		XSetWMNormalHints(Win.Dpy, Win.Window, &SizeHints);
	}

	XGetWindowAttributes(Win.Dpy, Win.Window, &Win.Attributes);

	// Select event mask
	long Mask = KeyPressMask | KeyReleaseMask | StructureNotifyMask;
	XSelectInput(Win.Dpy, Win.Window, Mask);

	Win.Gc = XCreateGC(Win.Dpy, Win.Window, 0, 0);

	XMapWindow(Win.Dpy, Win.Window);

	// By default, X11 reports a keyup event for each keydown, even while the key is physically pressed
	// By setting the second parameter to 1, a keyup event will be generated only when the physical keyboard key is released
	XkbSetDetectableAutoRepeat(Win.Dpy, 1, 0);
}

void VID_Shutdown()
{
	XCloseDisplay(Win.Dpy);
}

r_surface *R_CreateSurface(int Width, int Height)
{
	r_surface *Surface = malloc(sizeof *Surface);
	Surface->Width = Width;
	Surface->Height = Height;
	Surface->Pixels = malloc(Width * Height * 4);
	Surface->Texture = XCreateImage(
			Win.Dpy, 
			Win.Attributes.visual, 
			Win.Attributes.depth, 
			ZPixmap,
			0, Surface->Pixels,
			Width, Height,
			32,
			Width * 4);

	return Surface;
}

// Note: Do not free the memory returned by this function
void R_GetSurfacePixels(r_surface *Surface, void **Destination)
{
	*Destination = Surface->Pixels;
}

void R_ReleaseSurfacePixels(r_surface *Surface)
{
	// Stub
}

void R_BlitSurface(r_surface *Src, r_surface *Dst)
{
	// Perform software scaling when Dst is not NULL
	// This can be massively sped up with a lookup table
	r_surface *ToBlit = 0;
	if(Dst)
	{
		ToBlit = Dst;
		int x;
		int y;
		u32 *Dst32 = (u32 *)Dst->Pixels;
		u32 *Src32 = (u32 *)Src->Pixels;
		for(y = 0; y < Dst->Height; y++)
		{
			int sy = ((float)y / (float)Dst->Height) * (float)Src->Height;

			int sy_off = sy * Src->Width;
			int d_off = y * Dst->Width;
			for(x = 0; x < Dst->Width; x++)
			{
				int sx = ((float)x / (float)Dst->Width) * (float)Src->Width;
				
				int s = sy_off + sx;
				int d = d_off + x;
				Dst32[d] = Src32[s];
			}
		}
	}
	else
	{
		ToBlit = Src;
	}
	
	XPutImage(Win.Dpy, Win.Window, Win.Gc, ToBlit->Texture, 0, 0, 0, 0, ToBlit->Width, ToBlit->Height);
}

void VID_SwapBuffers()
{
	// Stub
}

// Keybindings are hardcoded
void VID_PollEvents()
{
	while(XPending(Win.Dpy))
	{
		XEvent Event;
		XNextEvent(Win.Dpy, &Event);

		switch(Event.type)
		{
			case KeyPress:
			{
				KeySym Sym = XkbKeycodeToKeysym(Event.xkey.display, Event.xkey.keycode, 0, 0);
				ks_event k_Event;
				k_Event.key = DFK_NONE;
				k_Event.pressed = 1;
				switch(Sym)
				{
					case XK_q:
					case XK_Escape:
						k_Event.key = DFK_QUIT;
						break;
					case XK_c:
						k_Event.key = DFK_DOUSE;
						break;
					case XK_v:
						k_Event.key = DFK_IGNITE;
						break;
				}
				KS_SendKey(&k_Event);
			}
			break;
		}
	}
}
