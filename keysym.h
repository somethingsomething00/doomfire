
#ifndef _KEYSYM_H_
#define _KEYSYM_H_

#define DFK_NONE 0
#define DFK_DOUSE 1
#define DFK_IGNITE 2
#define DFK_QUIT 3

typedef struct
{
	int pressed;
	int key;
} ks_event;

void KS_SendKey();
int KS_ProcessEvents(ks_event *Event);

#endif /* _KEYSYM_H_ */
