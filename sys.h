#ifndef _SYS_H_
#define _SYS_H_

void SYS_Init();
u64 SYS_GetPerformanceFrequency();
u64 SYS_GetPerformanceCounter();
double SYS_GetMonotonicSeconds();
void SYS_SleepMS(u64 milliseconds);


#endif /* _SYS_H_ */
