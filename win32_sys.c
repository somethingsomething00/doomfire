
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include "doomfire.h"


u64 s_PerformanceFrequency = 0;

void SYS_Init()
{
	LARGE_INTEGER Frequency;
	QueryPerformanceFrequency(&Frequency);
	s_PerformanceFrequency = Frequency.QuadPart;
}

u64 SYS_GetPerformanceFrequency()
{
	return s_PerformanceFrequency;
}

u64 SYS_GetPerformanceCounter()
{
	LARGE_INTEGER Counter;
	QueryPerformanceCounter(&Counter);
	return Counter.QuadPart;
}


double SYS_GetMonotonicSeconds()
{
	return (double)SYS_GetPerformanceCounter() / (double)SYS_GetPerformanceFrequency();
}

void SYS_SleepMS(u64 milliseconds)
{
	Sleep(milliseconds);
}

