# Doomfire

![thumbnail](./thumbnail.png)

```
The fire effect from the PS1 port of DOOM.
Also seen in DOOM 64.

I won't pretend to understand how it really works, 
but take a look at DoFire and SpreadFire in doomfire.c for the algorithms.

SDL2, X11, and Win32 versions can be built.
See build.sh on unix-like systems or build.bat on Windows systems.

On unix-like, the SDL2 backend is used by default.


Controls:
V........Ignite Fire
C........Douse Fire
ESC/q....Quit


Dependencies:
C compiler
SDL2 (or)
X11

Building:
--- unix-like ---
Build:
./build.sh

Run:
./doomfire

--- Windows ---
Build:
build.bat

Run:
doomfire.exe


Credits and inspiration:
https://fabiensanglard.net/doom_fire_psx/
```

## References

[Fabien Sanglard's page on the effect](https://fabiensanglard.net/doom_fire_psx/)

[SDL2 Documentation](https://wiki.libsdl.org/SDL2/CategoryAPI)

[X11 Documentation](https://tronche.com/gui/x/xlib/)
