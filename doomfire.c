#include "doomfire.h"

#include "vid.h"
#include "sys.h"

#define XS_IMPLEMENTATION
#include "xorshift.c"


// #include "sdl2_sys.c"
// #include "sdl2_vid.c"

// #include "linux_sys.c"
// #include "x11_vid.c"

// #include "common_vid.c"

// Credits to: https://fabiensanglard.net/doom_fire_psx/


// Coded in 2023-04


#define FIRE_WIDTH 320
#define FIRE_HEIGHT 200



// Globals
int Running = 1;
u8 *Fire8 = 0;

// Random state
xs_state32 State32 = {6666};


// Note: BGRA Little Endian
// B is the LSB
const u32 PAL[36] =
{
	[0] = 0xFF000707,
	[1] = 0xFF1f0707,
	[2] = 0xFF2f0f07,
	[3] = 0xFF470f07,
	[4] = 0xFF571707,
	[5] = 0xFF671f07,
	[6] = 0xFF771f07,
	[7] = 0xFF8f2707,
	[8] = 0xFF9f2f07,
	[9] = 0xFFaf3f07,
	[10] = 0xFFbf4707,
	[11] = 0xFFc74707,

	[12] = 0xFFDF4F07,
	[13] = 0xFFDF5707,
	[14] = 0xFFDF5707,

	[15] = 0xFFD75F07,
	[16] = 0xFFD7670F,

	[17] = 0xFFcf6f0f,
	[18] = 0xFFcf770f,
	[19] = 0xFFcf7f0f,
	[20] = 0xFFCF8717,
	[21] = 0xFFC78717,
	[22] = 0xFFC78F17,
	[23] = 0xFFC7971F,
	[24] = 0xFFBF9F1F,
	[25] = 0xFFBF9F1F,
	[26] = 0xFFBFA727,
	[27] = 0xFFBFA727,
	[28] = 0xFFBFAF2F,
	[29] = 0xFFB7AF2F,
	[30] = 0xFFB7B72F,
	[31] = 0xFFB7B737,
	[32] = 0xFFCFCF6F,
	[33] = 0xFFDFDF9F,
	[34] = 0xFFEFEFC7,
	[35] = 0xFFFFFFFF,
};

float RandF32()
{
	return xs_f32(&State32);
}

void SpreadFireSimple(int From)
{
	int To = From - FIRE_WIDTH;
	Fire8[To] = Fire8[From] - 1;
}

// Not sure how the buffer underflow/overflow crept in,
// but for now the Fire8 values are clamped in the SpreadFire functions
void SpreadFire(int From)
{
	int Random = (int)((RandF32() * 3)) & 3;
	int  To = From - FIRE_WIDTH - Random + 1;
	if(To < 0) To = 0;
	Fire8[To] = Fire8[From] - (Random & 1);
	if(Fire8[To] > 35) Fire8[To] = 0;
}

// Just playing with the numbers!
void SpreadFireWindy(int From)
{
	int Random = (int)((RandF32() * 3)) & 3;
	float WindFactor = RandF32() * 1;
	float Baseline = 0.5;
	int Wind = WindFactor < Baseline ? 0 : -10 * (1 - (WindFactor / (4 -  Baseline)));
	int  To = From - FIRE_WIDTH - Random + 1 + Wind;
	if(To < 0) To = 0;
	Fire8[To] = Fire8[From] - (Random & 1);
	if(Fire8[To] > 35) Fire8[To] = 0;
}



void DoFire()
{
	int x;
	int y;

	// Slight deviation from Fabian's code, we start the offset at FIRE_WIDTH immediately
	// instead of multiplying in the loop
	int pixel = FIRE_WIDTH;
	for(x = 0; x < FIRE_WIDTH; x++)
	{
		for(y = 1; y < FIRE_HEIGHT; y++)
		{
			SpreadFire(pixel);
			pixel++;
		}
	}
}

void AllocAndInitFire()
{
	Fire8 = calloc(FIRE_WIDTH * FIRE_HEIGHT, 1);
	IgniteFire();
}

void DouseFire()
{
	unsigned int BottomRow = FIRE_WIDTH * (FIRE_HEIGHT - 1);
	u8 *Pixels = &Fire8[BottomRow];
	memset(Pixels, 0, FIRE_WIDTH);
}

void IgniteFire()
{
	unsigned int BottomRow = FIRE_WIDTH * (FIRE_HEIGHT - 1);
	u8 *Pixels = &Fire8[BottomRow];
	memset(Pixels, 35, FIRE_WIDTH);
}

void Fire8ToFire32(u32 *Pixels32)
{
	int x;
	int y;

	for(x = 0; x < FIRE_WIDTH; x++)
	{
		for(y = 0; y < FIRE_HEIGHT; y++)
		{
			int Loc = y * FIRE_WIDTH + x;
			Pixels32[Loc] = PAL[Fire8[Loc]];
		}
	}
}

void ProcessEvents()
{
	ks_event Event;

	while(KS_ProcessEvents(&Event))
	{
		if(Event.pressed)
		{
			switch(Event.key)
			{
				case DFK_IGNITE:
				IgniteFire();
				break;

				case DFK_DOUSE:
				DouseFire();
				break;

				case DFK_QUIT:
				Running = 0;
				break;
			}
		}
	}
}



int main()
{
	// Accumulator for consistent fire updates regardless of framerate
	double T1, T2;
	float Accumulator = 0;

	VID_Init("DOOMFIRE", 0, 0, 640, 480, 0, 1);
	SYS_Init();

	r_surface *Framebuffer32 = R_CreateSurface(FIRE_WIDTH, FIRE_HEIGHT);
	r_surface *ScreenSurface32 = R_CreateSurface(640, 480);

	AllocAndInitFire();

	// Get one round of fire going
	DoFire();

	while(Running)
	{
		T1 = SYS_GetMonotonicSeconds();

		VID_PollEvents();
		ProcessEvents();


		int Updates = Accumulator / TARGET_SECONDS_PER_FRAME;
		Accumulator -= Updates * TARGET_SECONDS_PER_FRAME;
		for(int i = 0; i < Updates; i++)
		{
			DoFire();
		}

		void *Pixels32;
		R_GetSurfacePixels(Framebuffer32, &Pixels32);

		Fire8ToFire32(Pixels32);

		// Finished writing
		R_ReleaseSurfacePixels(Framebuffer32);

		R_BlitSurface(Framebuffer32, ScreenSurface32);
		VID_SwapBuffers();

		int slept = VID_Vsync(TARGET_SECONDS_PER_FRAME, T1, SYS_GetMonotonicSeconds(), &T2);
		if(!slept)
		{
			T2 = SYS_GetMonotonicSeconds();
		}

		float FrameTime = (T2 - T1);
		Accumulator += FrameTime;
	}

	VID_Shutdown();

	return 0;
}
