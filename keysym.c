#include "keysym.h"

int KS_ReadIndex = 0;
int KS_WriteIndex = 0;

#define KS_EVENT_COUNT 32

ks_event KS_Events[KS_EVENT_COUNT] = {0};

void KS_SendKey(ks_event *Event)
{
	KS_Events[KS_WriteIndex] = *Event;
	KS_WriteIndex++;
	KS_WriteIndex %= KS_EVENT_COUNT;
}

int KS_ProcessEvents(ks_event *Event)
{
	if(KS_ReadIndex == KS_WriteIndex) return 0;
	
	*Event = KS_Events[KS_ReadIndex];
	KS_ReadIndex++;
	KS_ReadIndex %= KS_EVENT_COUNT;
	
	return 1;
}