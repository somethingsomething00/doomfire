#ifndef _VID_H_
#define _VID_H_

extern int vid_software_vsync;

typedef void* r_surface;

void VID_Init(const char *Title, int x, int y, int Width, int Height, int Resizable, int Centered);
void VID_Shutdown();

r_surface *R_CreateSurface(int Width, int Height);
void *R_GetSurfacePixels(r_surface *Surface, void **Destination);
void R_ReleaseSurfacePixels(r_surface *Surface);
void R_BlitSurface(r_surface *Src, r_surface *Dst);
void VID_SwapBuffers();
void VID_PollEvents();
int VID_Vsync(double TargetSeconds, double T1, double T2, double *T3);








#endif /* _VID_H_ */
