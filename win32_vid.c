
#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include "doomfire.h"

int vid_software_vsync = 1;

typedef struct
{
	HWND Win;
	HDC Dc;
	
	int Width;
	int Height;
	
} win32_window;

win32_window Window = {0};

typedef struct
{
	int Width;
	int Height;
} win32_window_dimensions;


typedef struct
{
	BITMAPINFO BMI;
	int Width;
	int Height;
	unsigned char *Pixels;
} r_surface;

win32_window_dimensions Win32WindowDimensionsFromRect(RECT Rect)
{
	win32_window_dimensions Dimensions;
	
	Dimensions.Width = Rect.right - Rect.left;
	Dimensions.Height = Rect.bottom - Rect.top;
	
	return Dimensions;
}

// Keys are hardcoded
LRESULT CALLBACK Win32WindowProc(HWND Wnd, UINT Msg, WPARAM WParam, LPARAM LParam)
{
	LRESULT Result = 0;
	
	switch(Msg)
	{
		case WM_CLOSE:
		Running = 0;
		break;
		
		case WM_SIZE:
		{
			Window.Width = LOWORD(LParam);
			Window.Height = HIWORD(LParam);
		}
		
		case WM_KEYDOWN:
		{
			int Vkey = WParam;
			ks_event Event;
			Event.key = DFK_NONE;
			Event.pressed = 1;
			
			switch(Vkey)
			{
				case 'C':
				Event.key = DFK_DOUSE;
				break;
				
				case 'V':
				Event.key = DFK_IGNITE;
				break;
				
				case 'Q':
				case VK_ESCAPE:
				Event.key = DFK_QUIT;
				break;
			}
			
			KS_SendKey(&Event);
		}
		break;
		
		default:
		Result = DefWindowProc(Wnd, Msg, WParam, LParam);
	}
	
	return Result;
}

void VID_Init(const char *Title, int x, int y, int Width, int Height, int Resizable, int Centered)
{
	DWORD WindowFlags = WS_OVERLAPPEDWINDOW;
	const char *ClassName = "doomfire";
	WNDCLASS WindowClass = {0};
	
	WindowClass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	WindowClass.lpfnWndProc = Win32WindowProc;
	WindowClass.hCursor = LoadCursor(0, IDC_ARROW);
	WindowClass.hbrBackground = CreateSolidBrush(0x0);
	WindowClass.lpszClassName = ClassName;
	
	RegisterClass(&WindowClass);
	
	if(!Resizable)
	{
		// This disable resizing
		// WS_THICKFRAME is equivalent to WS_SIZEBOX
		WindowFlags &= ~(WS_THICKFRAME | WS_MINIMIZEBOX | WS_MAXIMIZEBOX);
	}
	
	Window.Win = CreateWindow(ClassName,
							  Title,
							  WindowFlags,
							  x,
							  y,
							  Width,
							  Height,
							  0,
							  0,
							  0,
							  0);
	
	
	// Move if needed, then show
	if(Centered)
	{
		MONITORINFO MI = {0};
		MI.cbSize = sizeof(MI);
		HMONITOR Monitor = MonitorFromWindow(Window.Win, MONITOR_DEFAULTTONEAREST);
		GetMonitorInfo(Monitor, &MI);
		
		win32_window_dimensions Dimensions = Win32WindowDimensionsFromRect(MI.rcMonitor);
		
		x = ((float)Dimensions.Width / 2) - ((float)Width / 2);
		y = ((float)Dimensions.Height / 2) - ((float)Height / 2);
		
		MoveWindow(Window.Win, x, y, Width, Height, FALSE);
	}
	
	ShowWindow(Window.Win, SW_SHOW);
	
	Window.Dc = GetDC(Window.Win);
	
	Window.Width = Width;
	Window.Height = Height;
}

void VID_Shutdown()
{
	DestroyWindow(Window.Win);
}

r_surface *R_CreateSurface(int Width, int Height)
{
	r_surface *Surface = malloc(sizeof *Surface);
	Surface->Width = Width;
	Surface->Height = Height;
	
	Surface->Pixels = malloc(Width * Height * 4);
	
	BITMAPINFOHEADER Header = {0};
	Header.biSize = sizeof(Header);
	Header.biWidth = Width;
	Header.biHeight = -Height;
	Header.biPlanes = 1;
	Header.biBitCount = 32;
	Header.biCompression = BI_RGB;
	
	Surface->BMI.bmiHeader = Header;
	
	return Surface;
}

// Note: Do not free the memory returned by this function
void R_GetSurfacePixels(r_surface *Surface, void **Destination)
{
	if(Destination) *Destination = Surface->Pixels;
}

void R_ReleaseSurfacePixels(r_surface *Surface)
{
	// Stub
}

void R_BlitSurface(r_surface *Src, r_surface *Dst)
{
	// Dst is not used here
	StretchDIBits(
				  Window.Dc,
				  0,         
				  0,         
				  Window.Width, 
				  Window.Height,
				  0,            
				  0,            
				  Src->Width,   
				  Src->Height,  
				  Src->Pixels,          
				  &Src->BMI,
				  DIB_RGB_COLORS,
				  SRCCOPY);
}

void VID_SwapBuffers()
{
	// Stub
}

void VID_PollEvents()
{
	MSG Message;
	while(PeekMessage(&Message, Window.Win, 0, 0, PM_REMOVE) > 0)
	{
		TranslateMessage(&Message);
		DispatchMessage(&Message);
	}
}