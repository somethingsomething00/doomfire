#include "doomfire.h"

#include <time.h>
#include <unistd.h>


void SYS_Init()
{
	// Stub
}

u64 SYS_GetPerformanceFrequency()
{
	return (u64)1e9;
}

u64 SYS_GetPerformanceCounter()
{
	struct timespec ts;
	clock_gettime(CLOCK_MONOTONIC, &ts);
	u64 seconds = ts.tv_sec;
	u64 ns = ts.tv_nsec;
	u64 total = ns + ((u64)1e9 * seconds);

	return total;
}

double SYS_GetMonotonicSeconds()
{
	return (double)SYS_GetPerformanceCounter() / (double)SYS_GetPerformanceFrequency();
}

void SYS_SleepMS(u64 milliseconds)
{
	usleep(milliseconds * 1000);
}
