#include "doomfire.h"
#include "vid.h"
#include "sys.h"



// Emulates vsync in software
// T3 contains the latest monotonic timestamp in seconds
// If this function performs a sleep, then T3 will contain the timestap after the sleep
// If not, T2  is copied into T3
// T3 can be NULL


// Returns: 1 if slept, 0 otherwise
int VID_Vsync(double TargetSeconds, double T1, double T2, double *T3)
{
	// Stub
	// The above version can be used if hardware vsync is not enabled
	if(!vid_software_vsync) return 0;
	
	int Slept = 0;
	double FrameTime = (T2 - T1);
	
	if(FrameTime < TargetSeconds)
	{
		double SleepFor = (TargetSeconds - FrameTime) * 1000.0;
		SYS_SleepMS(SleepFor);
		if(T3) *T3 = SYS_GetMonotonicSeconds();
		Slept = 1;
	}
	else
	{
		if(T3) *T3 = T2;
	}
	return Slept;
}

void VID_EnableSoftwareVsync(int Enable)
{
	vid_software_vsync = Enable ? 1 : 0;
}
