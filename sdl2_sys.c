#include "doomfire.h"

#include <SDL2/SDL.h>


u64 s_PerformanceFrequency = 0;

void SYS_Init()
{
	s_PerformanceFrequency = SDL_GetPerformanceFrequency();
}

u64 SYS_GetPerformanceFrequency()
{
	return s_PerformanceFrequency;
}

u64 SYS_GetPerformanceCounter()
{
	return SDL_GetPerformanceCounter();
}


double SYS_GetMonotonicSeconds()
{
	return (double)SYS_GetPerformanceCounter() / (double)SYS_GetPerformanceFrequency();
}

void SYS_SleepMS(u64 milliseconds)
{
	SDL_Delay(milliseconds);
}
