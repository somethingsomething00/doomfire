#!/bin/sh

cc="gcc"
src="doomfire.c"
src_common="common_vid.c keysym.c"
bin="doomfire"

compile()
{
	compile_command="$cc $src $src_common $src_extra $lib -O2 -o $bin"
	echo $compile_command
	$compile_command && printf "Built executable: $bin\n"
}

build_sdl2()
{
	lib="-lSDL2"
	src_extra="sdl2_vid.c sdl2_sys.c"
	compile
}

build_x11()
{
	lib="-lX11"
	src_extra="x11_vid.c linux_sys.c"
	compile
}

build_sdl2
# build_x11
