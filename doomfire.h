#ifndef _DOOMFIRE_H_
#define _DOOMFIRE_H_

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "keysym.h"

#define TARGET_SECONDS_PER_FRAME 0.0166667


typedef uint8_t u8;
typedef uint32_t u32;
typedef uint64_t u64;

extern int Running;

void DouseFire();
void IgniteFire();

#endif /* _DOOMFIRE_H_ */
